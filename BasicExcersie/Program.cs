﻿using System;

namespace BasicExcersie
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("For loop");

            for (int i = 1; i <= 10; i ++)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine("\n" +"Addition");

            int a = 10;
            int b = 5;

            Console.WriteLine("a + b = " + (a + b));

            Console.WriteLine("\n" + "String");

            string name = "Hooiyee";

            Console.WriteLine("My name is : " + name);

            Console.WriteLine("\n" + "Module");

            int c = 15;
            int d = 4;

            Console.WriteLine("c % d = " + (c % d));
            Console.WriteLine("hi");



        }
    }
}
